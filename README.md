## Von-Neuman vs Hardvard

``` plantuml

@startmindmap
*[#lightcoral] Von-Neumann vs Hardvard
 * Son modelos de arquitectura de computadoras
 * Un modelo es la Descripcion Funcional de la Implementacion De los Componentes de la Computadora
 *[#lightblue] Modelo Harvard 
  * Usa dos memorias 
   * Una para los datos
   * Otra para las instrucciones
  * Una instrucción se ejecuta en un solo ciclo.
  * La CPU puede acceder a instrucciones y leer/escribir al mismo tiempo.
  * La dirección de memoria física separada se utiliza para instrucciones y datos.
  * Se utiliza en:
   * Microcontroladores
    *_ Es
     * Circuito integrado programable, capaz\n de ejecutar las órdenes grabadas en su memoria.
    *_ Secompone de 
     * Unidad central de procesamiento
      * Unidad de Control
       * Controla el funcionamiento de la ALU, la memoria y los dispositivos\n de entrada/salida de la computadora, indicándoles cómo actuar\n ante las instrucciones del programa que acaba de leer desde la memoria.
      * Unidad artimeta-logica
       * Esta parte de la arquitectura está involucrada únicamente \nen la realización de operaciones aritméticas y lógicas sobre los datos.
     * Memoria 
      * El tipo de memoria utilizada en las memorias RAM de los microcontroladores es SRAM
      * Memoria de Datos
      * Memoria de Instrucicones
     * Periféricos de entrada/salida.
      *_ Utilizados para:
       * Puertos de comunicación
       * Conversor analógico/digital
       * Temporizadores y contadores
       * Comparadores
       * Entradas y salidas de propósito general
   * Procesamiento de señales Digitales.(DSP)
    *_ Es
     * Un dispositivo capaz de de procesar\nen tiempo real señales procedentes\nde diferentes fuentes.
     * Posee un conjunto de instrucciones, un hardware y un software \noptimizados para aplicaciones que requieran operaciones numéricas a muy alta velocidad
     * DSP puede trabajar con varios datos en paralelo 
 *[#lightblue] Modelo Von-Neuman
  * Fue Creado por Jhon Von-Neuman
   * Aporto enormemente a la computación
   * Nacio en Budapest Hungría el 28/12/1903
   * Participo en la Creacion del ENIAC
  * Caracteristicas
   * Usa una sola memoria
   * Se requieren dos ciclos de reloj para ejecutar una sola instrucción
   * La CPU no puede acceder a las instrucciones y leer/escribir al mismo tiempo.
   * Se utiliza en computadoras personales y computadoras pequeñas
   * La misma dirección de memoria física se utiliza para instrucciones y datos.
   * Establecio las Bases de las Arquitecturas  Modernas
  *_ Consiste en
   * Diferentes Bloques funcionales
    *_ Que son
     * Memoria Central
      * La memoria es un espacio físico capaz de almacenar datos,\n ordenar datos, guardar resultados, cargar operaciones, \nejecutar instrucciones, etc. 
       *_ Su funciones son:
        * -LOAD \n -ADD\n -MOVE\n -GOTO
     * Unidad central de procesamiento
      * Unidad aritmética lógica
       * Esta parte de la arquitectura está involucrada únicamente \nen la realización de operaciones aritméticas y lógicas sobre los datos.
      * Unidad de control
       * Controla el funcionamiento de la ALU, la memoria y los dispositivos\n de entrada/salida de la computadora, indicándoles cómo actuar\n ante las instrucciones del programa que acaba de leer desde la memoria.
     * Unidades de I/O
      * Es la colección de interfaces que usan las distintas unidades\n funcionales (subsistemas) de un sistema de procesamiento de información\n para comunicarse unas con otras, o las señales (información) enviadas a través\n de esas interfaces. Las entradas son las señales recibidas por la unidad, mientras\n que las salidas son las señales enviadas por ésta.

    * Interconectados Por Buses
     * Es un sistema digital que transfiere datos entre los componentes \nde una computadora. Está formado por cables o pistas en un circuito impreso,\n dispositivos como resistores y condensadores, además de circuitos integrados
      *_ Existen dos tipos de \ntransferencia en los buses
       * En Serie
        * El bus solamente es capaz de transferir los datos bit a bit.\n Es decir, el bus tiene un único cable que transmite la información.
       * Paralelo
        * El bus permite transferir varios bits simultáneamente, por ejemplo 8 bits
  * Tres Propiedades Del Modelo
   * Unico Espacio de Memoria
   * El Contenido es Accesible por Posición
   * La ejecucion de las instrucciones es Secuencial
  * Como funciona
   * Buscar
    * En este paso se obtienen las instrucciones desde la RAM \ny se las coloca en la memoria caché para que la unidad de control acceda a ellas.
   * Decodificar
    * La unidad de control decodifica las instrucciones de tal manera\n que la unidad aritmética lógica pueda comprenderlas, y luego las envía a la unidad aritmética lógica.
   * Ejecutar
    * La unidad lógica aritmética ejecuta las instrucciones y envía el resultado de nuevo a la memoria caché.
   * Almacenar
    * Una vez que el contador del programa indica detenerse, se descarga el resultado final a la memoria principal.

@endmindmap
```
## Supercomputadoras en México

```plantuml

@startmindmap
*[#lightcoral] Super Computadoras en México
 * UNAM 
  * obtuvo la primer gran Computadora en America Latina en 1958
   * Fue una IBM 650 
    * Tenia una Memoria de 2Ks
    * Funcionaba con Bulbos
    * Podia Procesar Mil Operaciones Aritmeticas Por Segundo
    * Leia la informacion a travez de tarjetas perforadas
  * En 1991 Obtuvo la CRAY 432
   * La primera SuperComputadora de America Latina
   * Estuvo  en Servicio por 10 años
  * En el 2007  puso en funcionamiento la SupeComputadora KanBalam
   * Se usa principalmente en Investigacion Cientifica
   * Puede realizar 7 billones de operaciones matematicas por segundo.
  * Actualmente la UNAM Utiliza la SuperComputadora Mizti
   * Cuenta con un Rendimiento Aproximado de 200 TeraFlops
   * Estudia otros temas como: la estructura del universo, sismos, comportamiento de particulas subatomicas,\nasi como el diseño de nuevos materiales, farmacos y reactores nucleares.
   * En 2012 Amplio su capacidad a mas de 8344 procesadores
   * tiene una memroria de 23 Terabytes.
 * Xiuhcoatl es una Supercomputadora,\nCreada en conjunto de Tres Universidades
  * Fue creada entre la UNAM , UAM, IPN
  * Se creo en el 2012
  * Tiene un rendimiento de 252 Teraflops
  * Es la segunda Supercomputadora mas potente de America Latina
  * Se conectan las 3 instituciones a traves de una red de fibra óptica.
  * Se usa para necesidades de investigación y\ndesarrollar servicios de computo de alto de desempeño
 * ABACUS I es una supercomputadora del CINVESTAV
  * Se creo en 2015
  * Pude llegar teoricamente a una Capacidad de 420 Teraflops
 * En 2008 se crea Aitzaloa   
  * En la UAM , con una capacidad de 18 Teraflops
 * BUAP
  * Una de las 5 computadoras más poderosas de América Latina
  * En tan solo 1 segundo puede ejecutar hasta 2000 millones de operaciones
  * Tiene un almacenamiento equivalente a mas de 5000 computadoras portatiles
  * Se usa principalmente para realizar simulaciones de
   * Formacion de una estrella
   * El avance de un huracan
   * Estructura del ADN
   * Visualizar una Molecula
  * Abre convocatorias para evaluar proyectos\nque requieran un procesamiento de grandes cantidades de datos.
@endmindmap

```
